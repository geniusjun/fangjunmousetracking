FROM node:8.10.0

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .
RUN npm install -g webpack@1.15.0 webpack-cli@2.0.10
RUN webpack

RUN mkdir -p /data/db

RUN apt-get update && apt-get -y install apt-transport-https &&\
	wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | apt-key add - &&\
	echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.2 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-4.2.list &&\
	apt-get update &&\
	apt-get install -y mongodb-org=4.2.0 mongodb-org-server=4.2.0 mongodb-org-shell=4.2.0 mongodb-org-mongos=4.2.0 mongodb-org-tools=4.2.0

EXPOSE 3001
EXPOSE 3000
CMD ["/bin/bash","-c","mongod -dbpath=/data/db & npm start"]
