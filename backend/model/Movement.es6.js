var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var movementSchema = new Schema({
    uuid: { type: String, required: true},
    x_coord: { type: Number, required: true },
    y_coord: { type: Number, required: true },
    game_id: { type: Number, min: 0, required: true },
    time: { type: String, required: true}
});

var Movement = mongoose.model('Movement', movementSchema);

module.exports = Movement;
