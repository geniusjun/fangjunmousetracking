var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const game_types = ['tally', 'ttb', 'delta'];

var participantSchema = new Schema({
    name: { type: String, required: true},
    uuid: { type: String, required: true},
    game_type: { type: String, required: true, enum: game_types},
    time: { type: String, required: true}
});

var Participant = mongoose.model('Participant', participantSchema);

module.exports = Participant;