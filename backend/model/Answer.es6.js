var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const ANSWERS = ['A', 'B'];
const game_types = ['tally', 'ttb', 'delta'];

var answerSchema = new Schema({
    uuid: { type: String, required: true},
    game_id: { type: Number, min: 0, required: true },
    correct_answer: { type: String, enum: ANSWERS, required: true },
    user_answer: { type: String, enum: ANSWERS, required: true },
    time: { type: String, required: true},
    game_type: { type: String, required: true, enum: game_types}
});

var Answer = mongoose.model('Answer', answerSchema);

module.exports = Answer;