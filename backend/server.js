require('app-module-path').addPath(__dirname);
var express = require('express');
var path = require('path');
var httpProxy = require('http-proxy');
var bodyParser = require('body-parser');
var api_router = require('./Routers/api_router.es6.js');

var proxy = httpProxy.createProxyServer();
var app = express();

var isProduction = process.env.NODE_ENV === 'production';
var host = process.env.APP_HOST || 'localhost';
var port = 3000;
var publicPath = path.resolve(__dirname, '..', 'public');

if (!isProduction) {
    // Any requests to localhost:3000/assets is proxied
    // to webpack-dev-server
    app.all(['/assets/*', '*.hot-update.json'], function (req, res) {
        proxy.web(req, res, {
            target: 'http://' + host + ':3001'
        });
    });
}

app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(express.static(publicPath));

// place your handlers here
app.use('/api', api_router);

app.get('/:type(ttb|tally|delta*)', function (req, res) {
    res.sendFile(path.join(publicPath, 'index.html'));
});

// It is important to catch any errors from the proxy or the
// server will crash. An example of this is connecting to the
// server when webpack is bundling
proxy.on('error', function (e) {
    console.log('Could not connect to proxy, please try again...');
});

app.listen(port, function () {
    console.log('Server running on port ' + port);
});
