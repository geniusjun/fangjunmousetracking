#!/usr/bin/env bash
mongoexport --db fangjun_prod --collection participants --type=csv --out participants.csv --fields  uuid,name,game_type,time
mongoexport --db fangjun_prod --collection answers --type=csv --out answers.csv --fields uuid,game_id,correct_answer,user_answer,time,game_type
mongoexport --db fangjun_prod --collection movements --type=csv --out movements.csv --fields uuid,x_coord,y_coord,time,game_id
