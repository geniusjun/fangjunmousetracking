import Game from '../model/game.es6.js'

class DemoDataStrategy1 {
  constructor() {
    const B = 'B';
    const A = 'A';

    const data = [
      ['2015','30000','1','20','2','2018','10000','2','2','1',B],
      ['2015','15000','1','20','1','2015','9000','1','2','0',B],
      ['2017','20000','1','10','0','2017','20000','2','1','1',A],
      ['2014','23000','2','10','1','2,014','235000','3','1','1',A],
      ['2010','23000','1','10','0','2000','29999','2','1','1',A]
    ];

    const game1_script = [
      {text:'You want to buy a used car. You searched online and found two potential cars. You want to choose the one that has higher value. The table below gives some information to aid your decision.'},
      {text:'Each value in the table can be used to predict the car\'s value.'},
      {text:'There are five cues for each of the two cars. The cues are ranked from top to bottom by their relevance to car’s value, with year the most relevant and accident report the least.'},
      {text:'You will be using a strategy to make your decisions. Keep in mind: the fewer years, mileages, number of previous owners, and number of accidents the car has, the better value.And more reviews the car has, the higher value.'},
      {text:'This strategy starts from searching for the most important cue: year in this case. If year values are different, we choose the most recent car. If they are equal, then we move on to the second most important cue, i.e. mileage, and choose the car with lower mileage.'},
      {text:'In the following examples, you will use this strategy to decide on the car that will have higher value.'},
      {text:'Move the mouse to the first cue: Car A\'s Year. Check out the value and then click Next.', highlights: {A: [0], B: []}},
      {text:'Move the mouse to the first cue: Car B\'s Year. Check out the value and then click Next.', highlights: {A: [], B: [0]}},
      {text:'Since Car B is more recent than Car A, B is our winner! Click Choose B'}
    ]; 

    const game2_script = [
      {text:'Move the mouse to the first cue: Car A\'s Year. Check out the value and then click Next.', highlights: {A: [0], B: []}},
      {text:'Move the mouse to the first cue: Car B\'s Year. Check out the value. Since year values of A and B are the same, we cannot make a decision, we move on to the second most important cue – Mileage. Click next.', highlights: {A: [], B: [0]}},
      {text:'Move the mouse to the second cue: Car A\'s Mileage. Check out the value and then click Next.', highlights: {A: [1], B: []}},
      {text:'Move the mouse to the second cue: Car B\'s Mileage. Check out the value and then click Next.', highlights: {A: [], B: [1]}},
      {text:'Since Car B has lower mileage, B is our winner! Choose B'}
    ];

    const game3_script = [
      {text:'Move the mouse to the first cue: Car A\'s Year. Check out the value and then click Next.', highlights: {A: [0], B: []}},
      {text:'Move the mouse to the first cue: Car B\'s Year. Check out the value. Since year values of A and B are the same, we cannot make a decision, we move on to the second most important cue – Mileage. Click next.', highlights: {A: [], B: [0]}},
      {text:'Move the mouse to the second cue: Car A\'s Mileage. Check out the value and then click Next.', highlights: {A: [1], B: []}},
      {text:'Move the mouse to the second cue: Car B\'s Mileage. Check out the value. Since mileage values of A and B are the same, we cannot make a decision, we move on to the third most important cue – Number of owners. Click next.', highlights: {A: [], B: [1]}},
      {text:'Move the mouse to the first cue: Car A\'s Number of owners. Check out the value and then click Next.', highlights: {A: [2], B: []}},
      {text:'Move the mouse to the first cue: Car B\'s Number of owners. Check out the value and then click Next.', highlights: {A: [], B: [2]}},
      {text:'Since Car A has fewer owners, A is our winner! Choose A'}
    ];

    const default_script = [
      {text:'Now please apply this strategy to the game below'}
    ];

    var game_data = [];
    data.forEach(function(single_data) {
      game_data.push(Game.fromCSV(single_data, default_script));
    });
    game_data[0].script = game1_script;
    game_data[1].script = game2_script;
    game_data[2].script = game3_script;

    this.data = game_data;
  }

  shouldShowData(gameNo, currentStep) {
    if (gameNo === 0 && currentStep <= 5) {
      return false;
    }

    return true;
  }

  validationFailureMsg() {
    return 'Please use the taught strategy to open the box.';
  }

  validateMovement(game, car, index) {
    // No restriction with we have instruction
    if (game.script.length > 1) {
      return true;
    }

    var access_history = game.access_history;
    for(let i = 0 ; i < index; i++) {
      if(access_history.A[i] === false || access_history.B[i] === false) {
        return false;
      }
    }
    return true;
  }
}

export default DemoDataStrategy1;
