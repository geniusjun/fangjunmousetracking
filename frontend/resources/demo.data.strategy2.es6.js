import Game from '../model/game.es6.js';
var _ = require('lodash');

class DemoDataStrategy2 {
  constructor() {
    const B = 'B';
    const A = 'A';

    const data = [
      ['2010','80,000','1','68','3','2016','15,000','2','59','1',B],
      ['2009','60,000','1','59','3','2011','48,000','1','100','1',B],
      ['2010','90,000','1','89','5','2002','135,000','5','20','6',A],
      ['2015','45,000','1','20','1','2014','68,000','3','12','1',A],
      ['2013','43,000','1','100','1','2011','65,000','1','2','6',A]
    ];
 
    const demoScript1 = [
      {text:'You want to buy a used car. You searched online and found two potential cars. You want to choose the one that has higher value. The table below gives some information to aid your decision.'},
      {text:'Each value in the table can be used to predict the value of the cars.'},
      {text:'There are five cues for each of the two cars. They are Year, Mileage, Number of Owners, Number of dealer\'s reviews and Accident report.'},
      {text:'You will be using a strategy to make your decisions.'},
      {text:'You apply this strategy by counting for each option the number of cues that have “good” values. In this case, you look at all 5 cues for the first car and count the number of “good” ones. Then you do the same for the second car. The car that has more good-valued cues wins.'},
      {text:'A good value should fall in the range specified in the column in between two cars. For example, if the Year is later than and equal to 2015, we consider it a relative young car and count it as good. If the Mileage is less than 50,000 miles, we think this cue has a good value, so on and so forth.'},
      {text:'Check out the values for the highlighted boxes by moving the mouse over it. Please check out car A’s cues from Year to Accident report in order.', highlights: {A: [0,1,2,3,4], B: []}},
      {text:'A good-valued cue counts as 1. A non-good-valued cue counts as 0. Car A’s Year is older than 2015 (0), Mileage is greater than 50,000 (0), Number of owners is less than and equal to 1 (1), Number of dealer\'s review is greater than 50 (1), and Accident report is greater than 1 case (0). Overall, Car A has 2 good-valued cues.', highlights: {A: [0,1,2,3,4], B: []}},
      {text:'Please check out car B’s cues from Year to Accident report in order. Car B has Year later than 2015 (1), Mileage less than 50,000 (1), Number of owner is greater than 1 (0), Number of dealer\'s review is greater than 50 (1), and Accident report is less than and equal to 1 (1). Overall, Car B has 4 good-valued cues.', highlights: {B: [0,1,2,3,4], A: []}},
      {text:'Because Car B has more good-valued cues (4) than Car A (2), we will make the bet that Car B will last longer. Choose B.'},
      {text:'Now please apply this strategy to the game that follows'}
    ];

    const default_script = [
      {text:'Now please apply this strategy to the game that follows'}
    ];

    var game_data = [];
    data.forEach(function(single_data) {
      game_data.push(Game.fromCSV(single_data, default_script));
    });
    game_data[0].script = demoScript1;
    this.data = game_data;
  }

  shouldShowData(gameNo, currentStep) {
    if (gameNo === 0 && currentStep <= 5) {
      return false;
    }

    return true;
  }

  validationFailureMsg() {
    return 'Please use the taught strategy to open the box.';
  }

  validateMovement(game, car, index) {
    // No restriction with we have instruction
    if (game.script.length > 1) {
      return true;
    }

    var access_history = game.access_history;
    if (car.label === 'A') {
      return _.uniq(access_history.B).length === 1;
    } else {
      return _.uniq(access_history.A).length === 1;
    }
  }
}

export default DemoDataStrategy2;
