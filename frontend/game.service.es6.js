GameService.$inject = ['$http'];

function GameService($http) {
  function sendMoveOverEvent(data) {
    var parameter = JSON.stringify(data);

    $http.post('/db/insertMoveOver', parameter);
  }

  function sendGame(data) {
    var parameter = JSON.stringify(data);
    $http.post('/api/answer', parameter);
  }

  function sendCoord(data) {
    var parameter = JSON.stringify(data);
    $http.post('/api/movement', parameter);
  }

  function saveUser(data) {
    var parameter = JSON.stringify(data);
    $http.post('/api/participant', parameter).error;
  }

  return {
    sendMoveOverEvent, sendGame, sendCoord, saveUser
  };
}

export default GameService;
