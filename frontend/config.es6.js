function config($stateProvider, $urlRouterProvider, $locationProvider, localStorageServiceProvider) {
  $locationProvider.html5Mode(true);

  $urlRouterProvider.otherwise("/");

  $stateProvider
    .state('main', {
      url: '/',
      template: require('./consent/consent.page.html'),
      controller: 'ConsentCtrl as vm',
      title: ''
    })
    .state('tally', {
      url: '/tally',
      template: require('./consent/consent.page.html'),
      controller: 'ConsentCtrl as vm',
      title: ''
    })
    .state('ttb', {
      url: '/ttb',
      template: require('./consent/consent.page.html'),
      controller: 'ConsentCtrl as vm',
      title: ''
    })
    .state('delta', {
      url: '/delta',
      template: require('./consent/consent.page.html'),
      controller: 'ConsentCtrl as vm',
      title: ''
    })
    .state('tally-game', {
      url: '/tally-game',
      template: require('./game.page.html'),
      controller: 'GameCtrl as vm',
      title: 'Let\'s play the game'
    })
    .state('ttb-game', {
      url: '/ttb-game',
      template: require('./game.page.html'),
      controller: 'GameCtrl as vm',
      title: 'Let\'s play the game'
    })
    .state('delta-game', {
      url: '/delta-game',
      template: require('./game.page.html'),
      controller: 'GameCtrl as vm',
      title: 'Let\'s play the game'
    })
    .state('tally-demo', {
      url: '/tally-demo',
      template: require('./demo/demo.page.html'),
      controller: 'DemoCtrl as vm',
      title: 'Instruction'
    })
    .state('delta-demo', {
      url: '/delta-demo',
      template: require('./demo/demo.page.html'),
      controller: 'DemoCtrl as vm',
      title: 'Instruction'
    })
    .state('ttb-demo', {
      url: '/ttb-demo',
      template: require('./demo/demo.page.html'),
      controller: 'DemoCtrl as vm',
      title: 'Instruction'
    })
    .state('transition', {
      url: '/tutorialcomplete/:strategy',
      template: require('./transition/tutorial-complete.page.html'),
      controller: 'TransitionCtrl as vm',
      title: 'Tutorial Complete'
    })
    .state('thank-you', {
      url: '/thank-you',
      template: require('./thank-you.page.html'),
      title: 'Thank you'
    });

  localStorageServiceProvider.setPrefix('app');
}

export default ['$stateProvider', '$urlRouterProvider', '$locationProvider', 'localStorageServiceProvider', config];
