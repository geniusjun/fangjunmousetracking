import Car from './car.es6.js';

class Game {
  constructor(carA, carB, script, answer) {
    this.carA = carA;
    this.carB = carB;
    this.script = script;
    this.answer = answer;
    this.access_history = {
      A: [false, false, false, false, false],
      B: [false, false, false, false, false]
    };

    //TODO: Delete after complete refactor
    this.tips = script;
  }

  static fromCSV(single_data, script) {
    var carA = new Car(single_data[0], single_data[1], single_data[2], single_data[3], single_data[4], 'A');
    var carB = new Car(single_data[5], single_data[6], single_data[7], single_data[8], single_data[9], 'B');
    return new Game(carA, carB, script, single_data[10]);
  }

  shouldShowBorder(car, index){}
}

export default Game;
