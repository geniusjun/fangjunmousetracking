class Car {
  constructor(mileage, year, price, resale_price, gas_efficiency, label) {
    this.mileage = mileage;
    this.year = year;
    this.price = price;
    this.resale_price = resale_price;
    this.gas_efficiency = gas_efficiency;
    this.visibility = {
      'Year': false,
      'Mileage': false,
      'Number of Previous Owners': false,
      'Number of Reviews': false,
      'Number of Accidents': false
    };
    this.can_show = [false, false, false, false, false];
    this.label = label;
  }

  static attribute_name() {
    return ['Year', 'Mileage', 'Number of Previous Owners', 'Number of Reviews', 'Number of Accidents'];
  }

  static attribute_name_value_map() {
    return {
      'Year':'mileage',
      'Mileage': 'year',
      'Number of Previous Owners':'price',
      'Number of Reviews':'resale_price',
      'Number of Accidents':'gas_efficiency'
    };
  }

  getAttribute(attribute_name) {
    let attribute = Car.attribute_name_value_map()[attribute_name];

    if (attribute in this) {
      return this[attribute];
    } else {
      throw 'Can not find attribute ${attribute_name}';
    }
  }

  setVisibility(attribute_name, should_show) {
    if (attribute_name in this.visibility) {
      this.visibility[attribute_name] = should_show;
    } else {
      throw 'Can not find attribute ${attribute_name}';
    }
  };

  shouldShowValue(attribute_name) {
    if (attribute_name in this.visibility) {
      return this.visibility[attribute_name];
    } else {
      throw 'Can not find attribute ${attribute_name}';
    }
  };
}

export default Car;
