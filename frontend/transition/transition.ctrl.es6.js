TransitionCtrl.$inject = ['$state'];

function TransitionCtrl($state) {
  var vm = this;
  console.log($state);

  vm.next = function() {
    switch($state.params.strategy) {
      case 'ttb':
        $state.go('ttb-game');
        break;
      case 'tally':
        $state.go('tally-game');
        break;
      case 'delta':
        $state.go('delta-game');
        break;
      default:
        break;
    }
  };
}

export default TransitionCtrl;
