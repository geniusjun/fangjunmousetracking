ConsentCtrl.$inject = ['$state', 'localStorageService', 'UUIDService', 'GameService'];

function ConsentCtrl($state, localStorageService, UUIDService, GameService) {
  var vm = this;

  vm.onAgree = function () {
    if (!vm.username) {
      alert('Please input your full name.');
      return;
    } else {
      var uuid = UUIDService.generateUUID();
      localStorageService.set('username', vm.username);
      localStorageService.set('uuid', uuid);
      GameService.saveUser({
        name: vm.username,
        uuid,
        game_type: $state.current.name
      });
    }

    switch($state.current.name) {
      case 'ttb':
        $state.go('transition', {strategy: 'ttb'});
        break;
      case 'tally':
        $state.go('tally-demo');
        break;
      case 'delta':
        $state.go('delta-demo');
      default:
        break;
    }
  }
}

export default ConsentCtrl;
